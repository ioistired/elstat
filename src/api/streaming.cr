require "json"
require "kemal"
require "logger"
require "uuid"
require "../helpers"

enum MessageOP
  Unsubscribe  = -1
  Subscribe    =  0
  Subscribed   =  1
  Unsubscribed =  2
  Data         =  3

  # incident specific
  IncidentNew    = 4
  IncidentUpdate = 5
  IncidentClose  = 6
end

enum ErrorCodes
  InvalidPayload = 4200
  TooMuch        = 4420
end

class Message
  JSON.mapping(
    op: Int32,
    channels: {type: Array(String), default: [] of String},
  )
end

def ws_sendchan(ws, opcode : MessageOP, channels : Array(String))
  ws.send({op: opcode.to_i32, channels: channels}.to_json)
end

add_context_storage_type(UUID)

def handle_ws_message(data, ws, env, manager)
  log = manager.context.log

  if data.size > 512
    raise Kemal::WebsocketError.new(
      ErrorCodes::TooMuch.to_i32, "Too much data")
  end

  begin
    message = Message.from_json(data)
    opcode = MessageOP.new(message.op)

    client_id = env.get? "client_id"

    if opcode == MessageOP::Subscribe
      # if the client is sending a Subscribe for the first time,
      # we allocate a new client ID and set it on env.
      # if not, we just resubscribe with the given list.
      if !client_id.is_a?(UUID)
        client_id = UUID.random
        log.info("new client: #{client_id}")
        env.set "client_id", client_id
      end

      subscribed = manager.subscribe ws, client_id, message.channels
      ws_sendchan(ws, MessageOP::Subscribed, subscribed)
    elsif opcode == MessageOP::Unsubscribe
      # check if we have a client id to start with.
      # this code would be way better if we had websocket blocking
      # operations (crystal-lang/crystal#5600) but nothing
      # happened there as of right now.

      if !client_id.is_a?(UUID)
        raise Kemal::WebsocketError.new(
          ErrorCodes::InvalidPayload.to_i32, "Not subscribed")
      end

      manager.unsubscribe client_id, message.channels
    else
      raise Kemal::WebsocketError.new(
        ErrorCodes::InvalidPayload.to_i32, "Invalid op code")
    end
  rescue ex : Kemal::WebsocketError
    raise ex
  rescue ex : Exception
    log.error("Websocket failed: #{ex}")
    raise Kemal::WebsocketError.new(4000, "err: #{ex.message}")
  end
end

def websocket_start(ws, env, manager)
  ws.on_message do |msg|
    handle_ws_message(msg, ws, env, manager)
  end

  ws.on_close do
    client_id = env.get? "client_id"

    if client_id.is_a?(UUID)
      unsubbed = manager.rm_websocket(client_id)
    end
  end
end
