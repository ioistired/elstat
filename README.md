# elstat

Status page and health check server.

## Installing

Requirements:

 - [Crystal] 0.29.0

[crystal]: https://crystal-lang.org/

```
git clone https://gitlab.com/elixire/elstat
cd elstat

# edit config.ini as you wish
cp config.example.ini config.ini

shards install
shards build --production
```

## Making a release tarball

You can make a self-contained tarball of elstat (containing a statically linked
binary compiled in release mode) like so:

```
make
```

## Running

As of right now, it is required to run elstat on the same directory as the
folder containing the source.

```
./bin/elstat
```

## Frontend

elstat is a server and does not serve any HTML. Available frontends:

- [shenlong](https://gitlab.com/elixire/shenlong)

Refer to each frontend's `README` file for installation instructions.

## Running tests

```
crystal spec
```
